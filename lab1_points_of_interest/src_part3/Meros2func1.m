%Angle detector (Harris-Stephens)

function [Triples, Laplacian, pixels] = Meros2func1(Im_bw, sigma, rho)

%2.1.1
n = ceil(3*sigma)*2 + 1;
m = ceil(3*rho)*2 + 1;
gaussian_sigma = fspecial('gaussian', n, sigma);
gaussian_rho = fspecial('gaussian', m, rho);

Im_s = imfilter(Im_bw, gaussian_sigma);

[gradient_x, gradient_y] = gradient(Im_s);

J1 = imfilter(gradient_x.^2, gaussian_rho);
J2 = imfilter(gradient_x .* gradient_y, gaussian_rho);
J3 = imfilter(gradient_y.^2, gaussian_rho);

%2.1.2
lplus = (J1 + J3 + sqrt((J1-J3).^2 + 4.*(J2.^2)))/2;
lminus = (J1 + J3 - sqrt((J1-J3).^2 + 4.*(J2.^2)))/2;

%2.1.3
k = 0.05;
criteriaR = (lminus.*lplus) - k.*((lminus+lplus).^2);

ns = ceil(3*sigma)*2+1;
B_sq = strel('disk',ns);
Cond1 = (criteriaR==imdilate(criteriaR,B_sq));

theta_corn = 0.005;
Cond2 = (criteriaR > theta_corn * max(max(criteriaR)));

pixels = Cond1 & Cond2;


Triples = zeros(sum(pixels(:)), 3);
[row, col]=find(pixels);
s = sigma * ones(length(row),1);
Triples = sortrows([col, row, s],1);

LoG_sigma = fspecial('log', n, sigma);
Laplacian = abs(sigma^2 * imfilter(Im_bw, LoG_sigma));

end

