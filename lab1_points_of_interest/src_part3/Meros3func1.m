%Multi-angle detector (Harris-Laplacian)

function [multi_tr] = Meros3func1(Im_bw, sigma, rho,s)

[Triples1, Laplacian1, ~] = Meros2func1(Im_bw, sigma, rho);
[Triples2, Laplacian2, ~] = Meros2func1(Im_bw, s*sigma, s*rho);
[Triples3, Laplacian3, ~] = Meros2func1(Im_bw, (s^2)*sigma, (s^2)*rho);
[Triples4, Laplacian4, ~] = Meros2func1(Im_bw, (s^3)*sigma, (s^3)*rho);

multi_tr = [];

Triples1_final = arrayfun(@(row) polyscale_1_neighb(Triples1(row,:),Laplacian1,Laplacian2), 1:length(Triples1(:,1)), 'uniformoutput', false);
Triples1_final = cat(1, Triples1_final{:});
multi_tr = cat(1, multi_tr, Triples1_final);

Triples2_final = arrayfun(@(row) polyscale_2_neighb(Triples2(row,:),Laplacian1,Laplacian2,Laplacian3), 1:length(Triples2(:,1)), 'uniformoutput', false);
Triples2_final = cat(1, Triples2_final{:});
multi_tr = cat(1, multi_tr, Triples2_final);

Triples3_final = arrayfun(@(row) polyscale_2_neighb(Triples3(row,:),Laplacian2,Laplacian3,Laplacian4), 1:length(Triples3(:,1)), 'uniformoutput', false);
Triples3_final = cat(1, Triples3_final{:});
multi_tr = cat(1, multi_tr, Triples3_final);

Triples4_final = arrayfun(@(row) polyscale_1_neighb(Triples4(row,:),Laplacian4,Laplacian3), 1:length(Triples4(:,1)), 'uniformoutput', false);
Triples4_final = cat(1, Triples4_final{:});
multi_tr = cat(1, multi_tr, Triples4_final);

multi_tr = multi_tr(any(multi_tr, 2), :); %clear rows with zeros (triples we dont need any more)

end

