%Multi-blobs detector (Hessian-Laplace)

function [multi_tr_blobs] = Meros3func2(Im_bw, sigma,s)

[Triples1b, Laplacian1b, ~] = Meros2func2(Im_bw, sigma);
[Triples2b, Laplacian2b, ~] = Meros2func2(Im_bw, s*sigma);
[Triples3b, Laplacian3b, ~] = Meros2func2(Im_bw, (s^2)*sigma);
[Triples4b, Laplacian4b, ~] = Meros2func2(Im_bw, (s^3)*sigma);

multi_tr_blobs = [];

Triples1_finalb = arrayfun(@(row) polyscale_1_neighb(Triples1b(row,:),Laplacian1b,Laplacian2b), 1:length(Triples1b(:,1)), 'uniformoutput', false);
Triples1_finalb = cat(1, Triples1_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples1_finalb);

Triples2_finalb = arrayfun(@(row) polyscale_2_neighb(Triples2b(row,:),Laplacian1b,Laplacian2b,Laplacian3b), 1:length(Triples2b(:,1)), 'uniformoutput', false);
Triples2_finalb = cat(1, Triples2_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples2_finalb);

Triples3_finalb = arrayfun(@(row) polyscale_2_neighb(Triples3b(row,:),Laplacian2b,Laplacian3b,Laplacian4b), 1:length(Triples3b(:,1)), 'uniformoutput', false);
Triples3_finalb = cat(1, Triples3_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples3_finalb);

Triples4_finalb = arrayfun(@(row) polyscale_1_neighb(Triples4b(row,:),Laplacian4b,Laplacian3b), 1:length(Triples4b(:,1)), 'uniformoutput', false);
Triples4_finalb = cat(1, Triples4_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples4_finalb);

multi_tr_blobs = multi_tr_blobs(any(multi_tr_blobs, 2), :); %clear rows with zeros (triples we dont need any more)

end

