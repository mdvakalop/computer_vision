%%
clear; clc;
addpath(genpath('libsvm-3.17'));
 
sigma=1.5;
p=2.5;
s = 1.5;
%% Feature Extraction
 
%detector_func = @(I) Meros3func1(I,sigma,p,s); %multi angles
%detector_func = @(I) Meros3func2(I,sigma,s); %multi blobs
detector_func = @(I) Meros2func4(I,sigma,s); %multi boxfilters 
 
%descriptor_func = @(I,points) featuresSURF(I,points);
descriptor_func = @(I,points) featuresHOG(I,points);
 
features = FeatureExtraction(detector_func,descriptor_func);
 
done = 'feature_done'
 
%% Image Classification
addpath(genpath('libsvm-3.17'));
parfor k=1:5
    %% Split train and test set
    [data_train,label_train,data_test,label_test]=createTrainTest(features,k);
    done = 'train test done'
    %% Bag of Words
    [BOF_tr,BOF_ts]=BagOfWords(data_train,data_test);
    done = 'bag of words done'
    %% SVM classification
    [percent(k),KMea] = svm(BOF_tr,label_train,BOF_ts,label_test(1:end-3));
    done='svm done'
    fprintf('Classification Accuracy: %f %%\n',percent(k)*100);
end
 
fprintf('Average Classification Accuracy: %f %%\n',mean(percent)*100);