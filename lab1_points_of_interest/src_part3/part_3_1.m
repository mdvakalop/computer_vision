clear
%clc
load('snrImgSet')

addpath(genpath('../detectors'));
addpath(genpath('../descriptors'));

sigma=2.0;
p=2.5;
s = 1.5;

I0 = ImgSet{1}{2};       % random image to get size
info = size(I0);         
%detector_func = @(I) Meros2func1(I,sigma,p); %gonies
%detector_func = @(I) Meros3func1(I,sigma,p,s); %multi gonies
%detector_func = @(I) Meros2func2(I,sigma); %blobs
detector_func = @(I) Meros3func2(I,sigma,s); %multi blobs
%detector_func = @(I) Meros2func4(I,sigma,s); %boxfilters multi
       
%descriptor_func = @(I,points) featuresSURF(I,points);
descriptor_func = @(I,points) featuresHOG(I,points);
        
[scale_error,theta_error] = evaluation(detector_func,descriptor_func)

