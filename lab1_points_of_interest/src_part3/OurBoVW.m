function [list,C] = OurBoVW(data_train,data_test)

data_tr = cell2mat(data_train(1));
data_te = cell2mat(data_test(1));
for i=2:length(data_train)
    data_tr = cat(1,data_tr,cell2mat(data_train(i)));
end  
for i=2:length(data_test)
    data_te = cat(1,data_te,cell2mat(data_test(i)));
end

numberofcenters = 200;
data_train_descr_size = length(data_tr);
idx = randperm(data_train_descr_size,ceil(data_train_descr_size/2));
data_selected = data_tr(idx,:);
[list,C] = kmeans(data_selected,numberofcenters,'start','uniform','emptyaction','singleton');

sdtrain = length(data_train);
sdtest = length(data_test);
data_test_descr_size = length(data_te);

distance_train = cell(sdtrain,1);
distance_test = cell(sdtest,1);

min = 100000;
for i=1:sdtrain                                                   %gia kathe ikona
    datax = cell2mat(data_train(i));
    sizex = size(datax);            
    min = 100000;
    for j=1:sizex(1)                                              %gia kathe perigrafiti
        for k=1:numberofcenters                                   %gia kathe kentro  
            if norm(datax(j,:)-C(k,:)) < min
                min = norm(datax(j,:)-C(k,:));
                min_k = k;
            end
        end
        distance_train{i} = [distance_train{i} min_k]; 
    end
end
for i=1:sdtest                                                    %gia kathe ikona
   datax = cell2mat(data_test(i));
    sizex = size(datax);
    min = 100000;
    for j=1:sizex(1)                                              %gia kathe perigrafiti
        for k=1:numberofcenters                                   %gia kathe kentro
             if norm(datax(j,:)-C(k,:)) < min
                min = norm(datax(j,:)-C(k,:));
                min_k = k;                
             end          
        end    
        distance_test{i} = [distance_test{i} min_k];
    end
end

%%
Htrain = cell(sdtrain,1);
for i=1:sdtrain
    Htrain{i} = histc(distance_train{i},1:numberofcenters);
end

Htest = cell(sdtrain,1);
for i=1:sdtrain
    Htest{i} = histc(distance_train{i},1:numberofcenters);
end

example=cell2mat(Htrain);
figure
bar(1:numberofcenters,example(1,:),'histc')

end