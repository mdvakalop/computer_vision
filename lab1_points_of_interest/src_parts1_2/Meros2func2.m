%blobs detector (Hessian)

function [Triples,Laplacian,pixels] = Meros2func2(Im_bw, sigma)

%2.3.1
n = ceil(3*sigma)*2+1;
gaussian_sigma = fspecial('gaussian', n, sigma);
Im_s = imfilter(Im_bw, gaussian_sigma);

[gradient_x, gradient_y] = gradient(Im_s);
[gradient_xx,gradient_yx] = gradient(gradient_x);
[gradient_xy,gradient_yy] = gradient(gradient_y);

criteria = gradient_xx.*gradient_yy - gradient_yx.*gradient_xy;

%2.3.2
ns = ceil(3*sigma)*2+1;
B_sq = strel('disk',ns);
Cond1 = (criteria==imdilate(criteria,B_sq));
 
theta_corn = 0.1;
Cond2 = (criteria > theta_corn * max(max(criteria)));

pixels = Cond1 & Cond2;

Triples = zeros(sum(pixels(:)), 3);
[row, col]=find(pixels);
s = sigma * ones(length(row),1);
Triples = sortrows([col, row, s],1);

%LoG_sigma = fspecial('log', n, sigma);
%Laplacian = abs(sigma^2*imfilter(Im_bw,LoG_sigma));

Laplacian = sigma^2*abs(gradient_xx+gradient_yy);
end

