%map function to avoid for loops in multi ange and multi blobs detectors

function b2 = polyscale_2_neighb(row, Lap1, Lap2, Lap3)
    x = row(1);
    y = row(2);
    if (Lap1(y, x) >= Lap2(y, x) || Lap3(y, x) >= Lap2(y, x))
        b2 = [0 0 0];
    else
        b2 = row;
    end
end
