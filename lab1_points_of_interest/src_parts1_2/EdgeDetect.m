function edges = EdgeDetect(Image,sigma,theta,LaplacType)

% 1.2.1
n = ceil(3*sigma)*2+1;
G = fspecial('gaussian', n, sigma);
LoG = fspecial('log', n , sigma);

%1.2.2
SmoothedImage = imfilter(Image, G);
figure(20);
imshow(SmoothedImage);
B = strel('disk', 1,0);

if LaplacType
    % linear
    Laplacian = imdilate(SmoothedImage, B) + imerode(SmoothedImage, B) - 2*SmoothedImage;
else
    % non-linear
    Laplacian = imfilter(Image, LoG);
end

%1.2.3
X = (Laplacian >= 0);
Y = imdilate(X, B) - imerode(X, B);
%edges = (Y==1);

%1.2.4
[smoothed_image_gradient_x, smoothed_image_gradient_y] = gradient(SmoothedImage);
metro = sqrt(smoothed_image_gradient_x.^2 + smoothed_image_gradient_y.^2);
max_metro = max(max(metro));
edges = (Y == 1) & (metro > theta * max_metro);

end

