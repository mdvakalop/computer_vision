%map function to avoid for loops in multi ange and multi blobs detectors

function b1 = polyscale_1_neighb(row, Lap1, Lap2)
    x = row(1);
    y = row(2);
    if (Lap1(y, x) <= Lap2(y, x))
        b1 = [0 0 0];
    else
        b1 = row;
    end

end
