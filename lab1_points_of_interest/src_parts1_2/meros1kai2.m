clear
clc
%% Meros 1: Anihnefsi Akmon se Grizes Eikones 
% 1.1. Dimiourgia Eikonon Eisodou

% 1.1.1 
I0 = im2double(imread('cv19_lab1_parts1_2_material/edgetest_19.png'));
figure(1);
imshow(I0);

% 1.1.2
% Imax = 1, Imin = 0 ara gia PSNR=20db exo sigma(var_gauss)=0.1 kai gia PSNR=10db exo sigma(var_gauss)=0.3 
var_gauss_20db = 0.1;
var_gauss_10db = 0.3;
m = 0;
I1 = imnoise(I0,'gaussian',m,var_gauss_20db);
figure(2);
imshow(I1);
I2 = imnoise(I0,'gaussian',m,var_gauss_10db);
figure(3);
imshow(I2);

%1.2. Ylopoiisi Algorithmon Anihnefsis Akmon
Dlinear_good = EdgeDetect(I1, 2.5, 0.2, true);
Dlinear_bad = EdgeDetect(I2, 3, 0.2, true);
Dnonlinear_good = EdgeDetect(I1, 2.5, 0.2, false);
Dnonlinear_bad = EdgeDetect(I2, 3.0, 0.2, false);
 
figure(4);
subplot(2,2,1);
imshow(Dlinear_good);
title('Dlinear good');
subplot(2,2,2);
imshow(Dlinear_bad);
title('Dlinear bad');
subplot(2,2,3);
imshow(Dnonlinear_good);
title('Dnonlinear good');
subplot(2,2,4);
imshow(Dnonlinear_bad);
title('Dnonlinear bad');

%1.3. Aksiologisi ton Apotelesmaton Anihnefsis Akmon

%1.3.1
B = strel('disk', 1,0);
theta_realedge = 0.2;
M = (imdilate(I0, B) - imerode(I0, B)); 
T = M > theta_realedge;
figure(6);
imshow(M);

%1.3.2-1.3.3
card_real_edges = sum(T(:));
card_linear_good = sum(Dlinear_good(:));
card_linear_bad = sum(Dlinear_bad(:));
card_nonlinear_good = sum(Dnonlinear_good(:));
card_nonlinear_bad = sum(Dnonlinear_bad(:));

card_real_lineargood = sum(T(:) & Dlinear_good(:));
card_real_linearbad = sum(T(:) & Dlinear_bad(:));
card_real_nonlineargood = sum(T(:) & Dnonlinear_good(:));
card_real_nonlinearbad = sum(T(:) & Dnonlinear_bad(:));
 
C_linear_good = (card_real_lineargood/card_real_edges + card_real_lineargood/card_linear_good)/2;
C_linear_bad = (card_real_linearbad/card_real_edges + card_real_linearbad/card_linear_bad)/2;
C_nonlinear_good = (card_real_nonlineargood/card_real_edges + card_real_nonlineargood/card_nonlinear_good)/2;
C_nonlinear_bad = (card_real_nonlinearbad/card_real_edges + card_real_nonlinearbad/card_nonlinear_bad)/2;

formatSpec = 'The quality of the linear method good image is %f.';
sprintf(formatSpec,C_linear_good)
formatSpec = 'The quality of the linear method bad image is %f.';
sprintf(formatSpec,C_linear_bad)
formatSpec = 'The quality of the nonlinear method good image is %f.';
sprintf(formatSpec,C_nonlinear_good)
formatSpec = 'The quality of the nonlinear method bad image is %f.';
sprintf(formatSpec,C_nonlinear_bad)

% 1.4. Efarmogi ton Algorithmon Anihnefsis Akmon se Pragmatikes Eikones

%1.4.1-1.4.2
I = im2double(imread('cv19_lab1_parts1_2_material/venice_edges.png'));
figure(5);
imshow(I);

opt_theta = 0.15;
opt_sigma = 0.5;
Dlinear = EdgeDetect(I, opt_sigma, opt_theta, true);
Dnonlinear = EdgeDetect(I, opt_sigma, opt_theta, false);
figure(6);
subplot(1,2,1);
imshow(Dlinear);
title('Dlinear');
subplot(1,2,2);
imshow(Dnonlinear);
title('Dnonlinear');

B = strel('disk', 1,0);
theta_realedge = 0.2;
Mreal = (imdilate(I, B) - imerode(I, B)); 
Treal = Mreal > theta_realedge;
figure(7);
imshow(Mreal);

card_real_edges = sum(Treal(:));
card_linear = sum(Dlinear(:));
card_nonlinear = sum(Dnonlinear(:));

card_real_linear = sum(Treal(:) & Dlinear(:));
card_real_nonlinear = sum(Treal(:) & Dnonlinear(:));

C_linear = (card_real_linear/card_real_edges + card_real_linear/card_linear)/2;
C_nonlinear= (card_real_nonlinear/card_real_edges + card_real_nonlinear/card_nonlinear)/2;

formatSpec = 'The quality of the linear method real image is %f.';
sprintf(formatSpec,C_linear)
formatSpec = 'The quality of the nonlinear method real image is %f.';
sprintf(formatSpec,C_nonlinear)

%% Meros 2: Anihnefsi Simion Endiaferontos(Interest Point Detection)
clc
clear

%2.1. Anihnefsi Gonion

Im0 = imread('cv19_lab1_parts1_2_material/balloons19.png');
Im1 = im2double(Im0);
Im2 = imread('cv19_lab1_parts1_2_material/sunflowers19.png');
Im3 = im2double(Im2);

figure(8);
imshow(Im1);

Im_bw = rgb2gray(Im1);
figure(9);
imshow(Im_bw);

sigma = 2.0;
rho = 2.5;
s = 1.5;

[Triples, Laplacian, pixels] = Meros2func1(Im_bw, sigma, rho);
figure(10);
interest_points_visualization(Im0, Triples);

%2.1. Poliklimakoti Anihnefsi Gonion

[Triples1, Laplacian1, pixels1] = Meros2func1(Im_bw, sigma, rho);
[Triples2, Laplacian2, pixels2] = Meros2func1(Im_bw, s*sigma, s*rho);
[Triples3, Laplacian3, pixels3] = Meros2func1(Im_bw, (s^2)*sigma, (s^2)*rho);
[Triples4, Laplacian4, pixels4] = Meros2func1(Im_bw, (s^3)*sigma, (s^3)*rho);

multi_tr = [];

Triples1_final = arrayfun(@(row) polyscale_1_neighb(Triples1(row,:),Laplacian1,Laplacian2), 1:length(Triples1(:,1)), 'uniformoutput', false);
Triples1_final = cat(1, Triples1_final{:});
multi_tr = cat(1, multi_tr, Triples1_final);

Triples2_final = arrayfun(@(row) polyscale_2_neighb(Triples2(row,:),Laplacian1,Laplacian2,Laplacian3), 1:length(Triples2(:,1)), 'uniformoutput', false);
Triples2_final = cat(1, Triples2_final{:});
multi_tr = cat(1, multi_tr, Triples2_final);

Triples3_final = arrayfun(@(row) polyscale_2_neighb(Triples3(row,:),Laplacian2,Laplacian3,Laplacian4), 1:length(Triples3(:,1)), 'uniformoutput', false);
Triples3_final = cat(1, Triples3_final{:});
multi_tr = cat(1, multi_tr, Triples3_final);

Triples4_final = arrayfun(@(row) polyscale_1_neighb(Triples4(row,:),Laplacian4,Laplacian3), 1:length(Triples4(:,1)), 'uniformoutput', false);
Triples4_final = cat(1, Triples4_final{:});
multi_tr = cat(1, multi_tr, Triples4_final);

multi_tr = multi_tr(any(multi_tr, 2), :); %clear rows with zeros (triples we dont need any more)

figure(11);
interest_points_visualization(Im0, multi_tr);
%%
% Anihnefsi Blobs

sigma = 2.0;

[Triples, Laplacian, pixels] = Meros2func2(Im_bw, sigma);
figure(12);
interest_points_visualization(Im0,Triples);

% Poliklimakoti Anihnefsi Blobs

[Triples1b, Laplacian1b, pixels1b] = Meros2func2(Im_bw, sigma);
[Triples2b, Laplacian2b, pixels2b] = Meros2func2(Im_bw, s*sigma);
[Triples3b, Laplacian3b, pixels3b] = Meros2func2(Im_bw, (s^2)*sigma);
[Triples4b, Laplacian4b, pixels4b] = Meros2func2(Im_bw, (s^3)*sigma);

multi_tr_blobs = [];

Triples1_finalb = arrayfun(@(row) polyscale_1_neighb(Triples1b(row,:),Laplacian1b,Laplacian2b), 1:length(Triples1b(:,1)), 'uniformoutput', false);
Triples1_finalb = cat(1, Triples1_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples1_finalb);

Triples2_finalb = arrayfun(@(row) polyscale_2_neighb(Triples2b(row,:),Laplacian1b,Laplacian2b,Laplacian3b), 1:length(Triples2b(:,1)), 'uniformoutput', false);
Triples2_finalb = cat(1, Triples2_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples2_finalb);

Triples3_finalb = arrayfun(@(row) polyscale_2_neighb(Triples3b(row,:),Laplacian2b,Laplacian3b,Laplacian4b), 1:length(Triples3b(:,1)), 'uniformoutput', false);
Triples3_finalb = cat(1, Triples3_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples3_finalb);

Triples4_finalb = arrayfun(@(row) polyscale_1_neighb(Triples4b(row,:),Laplacian4b,Laplacian3b), 1:length(Triples4b(:,1)), 'uniformoutput', false);
Triples4_finalb = cat(1, Triples4_finalb{:});
multi_tr_blobs = cat(1, multi_tr_blobs, Triples4_finalb);

multi_tr_blobs = multi_tr_blobs(any(multi_tr_blobs, 2), :); %clear rows with zeros (triples we dont need any more)

figure(13);
interest_points_visualization(Im0, multi_tr_blobs);

