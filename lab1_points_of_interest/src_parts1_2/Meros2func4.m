%Multi-box-filters detector

function multi_tr_bf = Meros2func4(I,sigma,s)

[Triples_bf1, Laplacian_bf1] = Meros2func3(I, sigma);
[Triples_bf2, Laplacian_bf2] = Meros2func3(I, s*sigma);
[Triples_bf3, Laplacian_bf3] = Meros2func3(I, (s^2)*sigma);
[Triples_bf4, Laplacian_bf4] = Meros2func3(I, (s^3)*sigma);

multi_tr_bf = [];

Triples_final_bf1 = arrayfun(@(row) polyscale_1_neighb(Triples_bf1(row,:),Laplacian_bf1,Laplacian_bf2), 1:length(Triples_bf1(:,1)), 'uniformoutput', false);
Triples_final_bf1 = cat(1, Triples_final_bf1{:});
multi_tr_bf = cat(1, multi_tr_bf, Triples_final_bf1);

Triples_final_bf2 = arrayfun(@(row) polyscale_2_neighb(Triples_bf2(row,:),Laplacian_bf1,Laplacian_bf2,Laplacian_bf3), 1:length(Triples_bf2(:,1)), 'uniformoutput', false);
Triples_final_bf2 = cat(1, Triples_final_bf2{:});
multi_tr_bf = cat(1, multi_tr_bf, Triples_final_bf2);

Triples_final_bf3 = arrayfun(@(row) polyscale_2_neighb(Triples_bf3(row,:),Laplacian_bf2,Laplacian_bf3,Laplacian_bf4), 1:length(Triples_bf3(:,1)), 'uniformoutput', false);
Triples_final_bf3 = cat(1, Triples_final_bf3{:});
multi_tr_bf = cat(1, multi_tr_bf, Triples_final_bf3);

Triples_final_bf4 = arrayfun(@(row) polyscale_1_neighb(Triples_bf4(row,:),Laplacian_bf4,Laplacian_bf3), 1:length(Triples_bf4(:,1)), 'uniformoutput', false);
Triples_final_bf4 = cat(1, Triples_final_bf4{:});
multi_tr_bf = cat(1, multi_tr_bf, Triples_final_bf4);

multi_tr_bf = multi_tr_bf(any(multi_tr_bf, 2), :); %clear rows with zeros (triples we dont need any more)

end

