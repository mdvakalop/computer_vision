%Meros 2.5 Box Filters & Integral Images
clear;
clc;
I = imread('cv19_lab1_parts1_2_material/balloons19.png');
I = im2double(I);
I1 = rgb2gray(I);
info = size(I1);

sigma = 2;
s = 1.5;

triples_bf = Meros2func3(I1,sigma);
figure(1);
interest_points_visualization(I,triples_bf);

triples_bfm = Meros2func4(I1,sigma,s);
figure(2);
interest_points_visualization(I,triples_bfm);
