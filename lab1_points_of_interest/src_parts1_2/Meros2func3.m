%box-filters detector

function [Triples,Laplacian] = Meros2func3(I,sigma)

[Lxx,Lyy,Lxy] = boxfilters(I,sigma);
criteriaR = Lxx.*Lyy - 0.81*(Lxy.^2);

ns = ceil(3*sigma)*2+1;
B_sq = strel('disk',ns);
Cond1 = (criteriaR==imdilate(criteriaR,B_sq));

theta_corn =0.1;
Cond2 = (criteriaR > theta_corn * max(max(criteriaR)));

pixels = Cond1 & Cond2;

Triples = zeros(sum(pixels(:)), 3);
[row, col]=find(pixels);
s = sigma * ones(length(row),1);
Triples = sortrows([col, row, s],1);

%Laplacian = sigma^2*abs(Lxx+Lyy);

LoG_sigma = fspecial('log', ns, sigma);
Laplacian = abs(sigma^2*imfilter(I,LoG_sigma));

end

