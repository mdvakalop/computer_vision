function [idx] = OurBoVW(data_train,data_test)

data = cell2mat(data_train(1));
for i=2:length(data_train)
    data = cat(1,data,cell2mat(data_train(i)));
end  

data1 = data(1:length(data)/2);
idx = kmeans(data1,500);

end

