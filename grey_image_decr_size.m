cat = rgb2gray(imread('cat.jpg')); %8-bit/pixel rescale image loaded

%a) 8 isoheight level curves uniformly in [0,255]
m = 8;
[index, heights] = InitHeights(m);

figure(1)
imcontour(cat, heights, 'k');
print -djpeg90 -r300 isoheight_curves_cat.jpg;


%b) reconstruction
%map each pixel-value to the correct level value according to height vector
reconstr_cat = uint8(arrayfun(@(pixel) LevelMap(pixel,heights,index), cat)); 
figure(2)
imshow(reconstr_cat);
print -djpeg90 -r300 reconstructed_cat_8.jpg;


%c) psnr for m=4,8,16,32,64,128 (6 values)
psnr = zeros(1,6);

%m=8
psnr(2) = 20*log10(norm(double(cat - reconstr_cat))/255);

%m=4
m = 4;
[index, heights] = InitHeights(m);
reconstr_cat = uint8(arrayfun(@(pixel) LevelMap(pixel,heights,index), cat)); 
psnr(1) = 20*log10(norm(double(cat - reconstr_cat))/255);

%m=16
m = 16;
[index, heights] = InitHeights(m);
reconstr_cat = uint8(arrayfun(@(pixel) LevelMap(pixel,heights,index), cat)); 
psnr(3) = 20*log10(norm(double(cat - reconstr_cat))/255);

%m=32
m = 32;
[index, heights] = InitHeights(m);
reconstr_cat = uint8(arrayfun(@(pixel) LevelMap(pixel,heights,index), cat)); 
psnr(4) = 20*log10(norm(double(cat - reconstr_cat))/255);

%m=64
m = 64;
[index, heights] = InitHeights(m);
reconstr_cat = uint8(arrayfun(@(pixel) LevelMap(pixel,heights,index), cat)); 
psnr(5) = 20*log10(norm(double(cat - reconstr_cat))/255);

%m=128
m = 128;
[index, heights] = InitHeights(m);
reconstr_cat = uint8(arrayfun(@(pixel) LevelMap(pixel,heights,index), cat)); 
psnr(6) = 20*log10(norm(double(cat - reconstr_cat))/255);

%plot results
figure(8)
bar([4 8 16 32 64 128], psnr);
print -djpeg90 -r300 psnr.jpg;


function [index, heights] = InitHeights(m)
    %begin from index/2 and grow with step=index to find uniform levels
    index = 256/m; 
    heights = zeros(1,m);
    start = index/2;
    heights(1) = start;

    for i=2:m
        next = start + index;
        start = next;
        heights(i) = next;
    end
end

function pixel_value = LevelMap(pixel, heights, index)    
    idx = floor((pixel + index/2)/index);
    if idx == 0
        pixel_value = 0;
    else
        pixel_value = heights(idx);
    end
end



